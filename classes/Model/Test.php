<?php
class Model_Test extends \DB_PDO_Table{

	protected $name         = 'test';
	protected $columns      = array(
		'id',
		'title',
	);
	protected $indices      = array(
		'title',
	);
	protected $primaryKey   = 'id';
	protected $fetchMode    = PDO::FETCH_OBJ;

}
