<?php
class Client{

	protected $config;
	protected $dbc;
	protected $tables			= array();
	protected $models			= array();

	public function __construct(){
		$rootPath		= dirname( dirname( __FILE__ ) ).'/';
		if( !file_exists( $rootPath.'config/config.ini' ) )
			throw new RuntimeException( 'Please copy config/config.ini.dist to config/config.ini and apply your environmental settings, first!' );
		$this->config	= new \ADT_List_Dictionary( parse_ini_file( $rootPath.'config/config.ini' ) );

		$dba			= (object) $this->config->getAll( 'database.' );
		$dsn			= new \DB_PDO_DataSourceName( 'mysql', $dba->name );
		$this->dbc		= new \DB_PDO_Connection( $dsn, $dba->user, $dba->pass );
		$this->dbc->setErrorLogFile( 'log/db.error.log' );
//		$this->dbc->setStatementLogFile( 'log/db.query.log' );
		$this->loadTablesInDatabase();
	}

	protected function loadTablesInDatabase(){
		$this->tables	= $this->dbc->query( "SHOW TABLES" )->fetchAll( PDO::FETCH_COLUMN );
	}

	public function hasTable( $table ){
		return in_array( $table, $this->tables );
	}

	public function getTable( $table ){
		if( isset( $this->models[$table] ) )
			return $this->models[$table];
		if( !$this->hasTable( $table ) )
			throw new DomainException( 'Table "'.$table.'" is not existing in database' );
		$name		= str_replace( " ", "", ucwords( str_replace( "_", " ", $table ) ) );
		$className	= '\\Model_'.$name;
		if( !class_exists( $className ) )
			throw new DomainException( 'Model class "'.$className.'" for table "'.$table.'" is not existing' );
		$object		= new $className( $this->dbc );
		$this->models[$table]	= $object;
		return $object;
	}

	public function getTables(){
		return $this->tables;
	}

	public function getFromTable( $table, $conditions = array() ){
		$model		= $this->getTable( $table );
		$result		= $model->getAll( $conditions );
		return $result;
	}

	public function getFromTableById( $table, $id, $fieldName = NULL ){
		$model		= $this->getTable( $table );
		$result		= $model->get( $id, (string) $fieldName );
		return $result;
	}

	public function setInTableById( $table, $id, $data = array(), $stripTags = TRUE ){
		$model		= $this->getTable( $table );
		$result		= $model->edit( $id, $data, $stripTags );
		return $result;
	}

	public function setInTable( $table, $indices = array(), $data = array(), $stripTags = TRUE ){
		$model		= $this->getTable( $table );
		$result		= $model->editByIndices( $indices, $data, $stripTags );
		return $result;
	}
}
