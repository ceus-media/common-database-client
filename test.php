<?php
(@include 'vendor/autoload.php') or die('Please use composer to install required packages.' . PHP_EOL);

error_reporting( E_ALL );
\Loader::registerNew( 'php', NULL, './classes/' );
new UI_DevOutput;

$config = new \ADT_List_Dictionary( parse_ini_file( 'config/config.ini' ) );
$dba    = (object) $config->getAll( 'database.' );

$dsn    = new \DB_PDO_DataSourceName( 'mysql', $dba->name );
$dbc    = new \DB_PDO_Connection( $dsn, $dba->user, $dba->pass );
$dbc->setErrorLogFile( 'log/db.error.log' );
$dbc->setStatementLogFile( 'log/db.query.log' );

$model	= new \Model_Test( $dbc );
//print_r( $model->getAll() );
print_m( $model->getAll() );

