# CeusMedia Demo

## Datenbank mit PHP und CeusMedia/Common mittels PDO-Binding

### Vorbedingungen
1. composer installieren (bestenfalls global)
2. Pakete installieren mit 'composer install'

### Vorbereitung
1. config/config.ini: DB-Zugang konfigurieren
2. Datenbank anlegen
3. config/schema.sql importieren

### Beispiel: test.php

- Script beinhaltet alles, was nötig ist
- nutzt eine Model-Klasse, die eine Table-Klasse konfiguriert
- Table-Klasse beinhaltet alle möglichen Methoden
- intern werden Table-Reader- und Table-Writer-Klassen genutzt
- https://github.com/CeusMedia/Common/blob/master/src/DB/PDO/Table.php

### Beispiel: client.php

- Client-Klasse beinhaltet große Teile von test.php
- nimmt damit die meiste Arbeit ab
- Client-Instanz stellt DB-Verbindung her
- Client-Instanz kann Model-Instanzen auf Table-Name erzeugt
- Operation dann an der Table-Instanz
- Shortcut 'Client::getFromTable' für kürzeste SELECT-Abfrage mit WHERE-Bedingungen

