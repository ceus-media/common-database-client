<?php

(@include 'vendor/autoload.php') or die('Please use composer to install required packages.' . PHP_EOL);
\Loader::registerNew( 'php', NULL, './classes/' );
new UI_DevOutput;

try{

	//  Client stellt DB-Verbindung her
	//  Konfiguration: config/config.ini
	$client	= new \Client();

	//  Normaler Weg
	//  Table-Instanz vom Client erzeugen lassen
	//  Operationen dann an der Table-Instanz
	$table	= $client->getTable( 'test' );

	print( PHP_EOL."All entries of table 'test':" );
	print_m( $table->getAll() );

	//  Cursor auf ID
	//  Beispiel einer kurzen Form einer SELECT-Abfrage mit ID-Bedingung
	print( PHP_EOL."Now, entries of table 'test' with ID 1:" );
	print_m( $client->getFromTableById( 'test', 1 ) );
	print( "... which is the same result as getting all entries, atleast in this demo an in its original state.".PHP_EOL );

	//  Shortcut
	//  Beispiel einer kurzen Form einer SELECT-Abfrage mit WHERE-Bedingungen
	//  in diesem Fall sogar mit einem LIKE
	print( PHP_EOL."Now, all entries of table 'test' starting with 'te':" );
	print_m( $client->getFromTable( 'test', array( 'title' => 'te%' ) ) );
	print( "... which is the same result as getting all entries, atleast in this demo an in its original state.".PHP_EOL );
	print( PHP_EOL."Now! Go and play with it!" );
	print( PHP_EOL."Extend classes/Client.php after your needs and make your calls in client.php to set whats happening.".PHP_EOL );
	print( PHP_EOL."Have fun ;-)".PHP_EOL );
}
catch( Exception $e ){
	if( getEnv( 'HTTP_HOST' ) ){
		UI_HTML_Exception_Page::display( $e );
		exit;
	}
	print( $e->getMessage().PHP_EOL.PHP_EOL );
}
